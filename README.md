# Phosh

As of 2021-08-02 Upstream development moved to GNOME's gitlab. The new location for code
and issues is at https://gitlab.gnome.org/World/Phosh/phosh.

The packaging for the Librem 5 and PureOS
lives at https://source.puri.sm/Librem5/debs/pkg-phosh.
